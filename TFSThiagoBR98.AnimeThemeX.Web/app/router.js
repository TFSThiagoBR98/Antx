import EmberRouter from '@ember/routing/router';
import config from './config/environment';

const Router = EmberRouter.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('about');
  this.route('copyright');
  this.route('search', { path: '/search/:searchquery' });

  this.route('catalogs', { path: '/catalogs' });
  this.route('catalog', { path: '/catalogs/:catalogid' }, function() {
    this.route('season', { path: '/:seasonid' });
  });

  this.route('anime', { path: '/anime/:animeid' }, function() {
    this.route('theme', { path: 'theme/:themeid'});
  });

  this.route('playlist', function() {
    this.route('play', { path: 'play/:index/:themeid' });
    this.route('mal');
    this.route('kitsu');
    this.route('season');
    this.route('year');
    this.route('create');

    this.route('rank', function() {
      this.route('popularity');
      this.route('score');
      this.route('airing');
    });
    this.route('random');
    this.route('anilist');
  });

  this.route('rank', function() {
    this.route('popularity');
    this.route('airing');
    this.route('score');
  });
  this.route('combat');
});

export default Router;
