import $ from 'jquery';
import { Promise } from 'rsvp';

export function initialize(/* application */) {
    var current_progress = 0;
    var maxProgress = 100;
    var updateProgress  = (function(toAdd) {
        current_progress += toAdd;
        $("#pgbr")
            .css("width", current_progress + "%")
            .attr("aria-valuenow", current_progress)
            .text(current_progress + "% Complete");
    });
    var loadDatabase = (function() {
        Promise.all([
            $.getJSON("/api/Version.json").then(function(data){
                window.$('#ver').text(data.GeneratedTime);
                localforage.setItem('version', data, function (err) {
                    if (err != null) {
                        alert("ERROR: The webapp can't cache database, use this app in storage compatible Code:" + err);
                    }
                    updateProgress(maxProgress/12);
                });
            }),
            $.getJSON("/api/StageII.json").then(function(data){
                localforage.setItem('collectionTable', data, function (err) {
                    if (err != null) {
                        alert("ERROR: The webapp can't cache database, use this app in storage compatible Code:" + err);
                    }
                    updateProgress(maxProgress/12);
                });
            }),
            $.getJSON("/api/Search.Map.json").then(function(data){
                localforage.setItem('searchTable', data, function (err) {
                    if (err != null) {
                        alert("ERROR: The webapp can't cache database, use this app in storage compatible Code:" + err);
                    }
                    updateProgress(maxProgress/12);
                });
            }),
            $.getJSON("/api/Anime.Map.json").then(function(data){
                localforage.setItem('serieTable', data, function (err) {
                    if (err != null) {
                        alert("ERROR: The webapp can't cache database, use this app in storage compatible Code:" + err);
                    }
                    updateProgress(maxProgress/12);
                });
            }),
            $.getJSON("/api/Kitsu.Map.json").then(function(data){
                localforage.setItem('kitsuTable', data, function (err) {
                    if (err != null) {
                        alert("ERROR: The webapp can't cache database, use this app in storage compatible Code:" + err);
                    }
                    updateProgress(maxProgress/12);
                });
            }),
            $.getJSON("/api/Anilist.Map.json").then(function(data){
                localforage.setItem('anilistTable', data, function (err) {
                    if (err != null) {
                        alert("ERROR: The webapp can't cache database, use this app in storage compatible Code:" + err);
                    }
                    updateProgress(maxProgress/12);
                });
            }),
            $.getJSON("/api/MyAnimeList.Map.json").then(function(data){
                localforage.setItem('malTable', data, function (err) {
                    if (err != null) {
                        alert("ERROR: The webapp can't cache database, use this app in storage compatible Code:" + err);
                    }
                    updateProgress(maxProgress/12);
                });
            }),
            $.getJSON("/api/Top.Popularity.json").then(function(data){
                localforage.setItem('rankPopTable', data, function (err) {
                    if (err != null) {
                        alert("ERROR: The webapp can't cache database, use this app in storage compatible Code:" + err);
                    }
                    updateProgress(maxProgress/12);
                });
            }),
            $.getJSON("/api/Top.Airing.json").then(function(data){
                localforage.setItem('rankAirTable', data, function (err) {
                    if (err != null) {
                        alert("ERROR: The webapp can't cache database, use this app in storage compatible Code:" + err);
                    }
                    updateProgress(maxProgress/12);
                });
            }),
            $.getJSON("/api/List.Collections.json").then(function(data){
                localforage.setItem('mappings', data, function (err) {
                    if (err != null) {
                        alert("ERROR: The webapp can't cache database, use this app in storage compatible Code:" + err);
                    }
                    updateProgress(maxProgress/12);
                });
            }),
            $.getJSON("/api/List.Anime.json").then(function(data){
                localforage.setItem('serieMappings', data, function (err) {
                    if (err != null) {
                        alert("ERROR: The webapp can't cache database, use this app in storage compatible Code:" + err);
                    }
                    updateProgress(maxProgress/12);
                });
            }),
            $.getJSON("/api/Top.Score.json").then(function(data){
                localforage.setItem('rankScoreTable', data, function (err) {
                    if (err != null) {
                        alert("ERROR: The webapp can't cache database, use this app in storage compatible Code:" + err);
                    }
                    updateProgress(maxProgress/12);
                });
            })
        ]).then(function() {
            window.$('#loading').modal('hide');
        });
    });
    localforage.getItem('version', function (err, version) {
        window.$('#loading').modal({backdrop: 'static', keyboard: false});
        if (err != null) {
            alert("Error while loading data: " + err);
        } else if (version == null) {
            // Download database here
            localforage.clear(loadDatabase());
        } else {
            // Check latest version
            $.getJSON("/api/Version.json").then(function(latest){
                localforage.getItem('version', function (err, version) {
                    if (err != null) {
                        alert("ERROR: The webapp can't cache database, use this app in storage compatible Code:" + err);
                    }
                    if (version.GeneratedTime == latest.GeneratedTime) {
                        updateProgress(maxProgress);
                        window.$('#ver').text(version.GeneratedTime);
                        window.$('#loading').modal('hide');
                    } else {
                        localforage.clear(loadDatabase());
                    }
                });
            });
        }
    });
}

export default {
  initialize
};
