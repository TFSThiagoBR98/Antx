import Route from '@ember/routing/route';
import { Promise } from 'rsvp';

export default Route.extend({
    model(params) {
        var pag = this.paramsFor('catalog');
        return new Promise(function(resolve) {
            localforage.getItem('collectionTable', function (err, value) {
                if (err != null) {
                    alert("ERROR: The webapp can't load cache database, use this app in storage compatible Code:" + err);
                } else {
                    value.forEach(element => {
                        if (element.CollectionName == pag.catalogid) {
                            resolve(element.Seasons[params.seasonid]);
                        }
                    });
                }
            });
        });
    }
});
