import Route from '@ember/routing/route';
import $ from 'jquery';
import { schedule } from '@ember/runloop';

export default Route.extend({
    model(params){
        return this.modelFor('anime').Themes[params.themeid];
    },
    actions: {
        prevTheme() {
            if (parseInt(this.paramsFor(this.routeName).themeid) > 0) {
                window.player.destroy();
                window.$("#modal-player").empty();
                window.$("#modal_player").off("hidden.bs.modal");
                window.$("#modal_player").modal('hide');
                this.transitionTo(this.routeName, parseInt(this.paramsFor(this.routeName).themeid) - 1);
            }
            return true;
        },
        downVideo() {
            window.open(window.player.media.currentSrc, 'Download');
        },
        nextTheme() {
            if (parseInt(this.paramsFor(this.routeName).themeid) < (this.modelFor('anime').Themes.length - 1)) {
                window.player.destroy();
                window.$("#modal-player").empty();
                window.$("#modal_player").off("hidden.bs.modal");
                window.$("#modal_player").modal('hide');
                this.transitionTo(this.routeName, (parseInt(this.paramsFor(this.routeName).themeid) + 1));
            }
        },
        willTransition() {
            window.player.destroy();
            window.$("#modal-player").empty();
            window.$("#modal_player").off("hidden.bs.modal");
            window.$("#modal_player").modal('hide');
        },
        didTransition() {
            var classe = this;
            schedule('afterRender', this, function () {
                window.$("#modal-player").append($('<video id="player" class="player-theme" autoplay playsinline controls></video>'));
                classe.modelFor(classe.routeName).Videos.forEach(element => {
                    window.$("#player").append($('<source src="' + element.Link + '" type="video/webm" label="' + element.Name + '" height="' + element.Quality + '">'));
                });
                window.$("#modal_player").on("hidden.bs.modal", function () {
                    classe.transitionTo('anime');
                });
                
                window.$("#modal_player").modal();
                window.player = new window.Plyr('#player', {
                    autoPlay: true
                });
                window.player.elements.container.classList.add("player-theme");
            });
            return true;
        }
    }
});
