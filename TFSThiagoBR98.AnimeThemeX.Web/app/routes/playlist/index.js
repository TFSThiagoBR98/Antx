import Route from '@ember/routing/route';
import RSVP, { Promise } from 'rsvp';

export default Route.extend({
    model() {
        var route = this;
        return new Promise(function(resolve) {
            localforage.getItem('playlist', function (err, playlist) {
                if (err != null) {
                    alert("ERROR: The webapp can't load cache database, use this app in storage compatible Code: " + err);
                } else if (playlist == null) {
                    alert("ERROR: A playlist is not created, use the menu to create a playlist");
                    route.transitionTo('index');
                } else {
                    resolve(new RSVP.hash({
                        count: playlist.length,
                        playlist: playlist
                    }));
                }
            });
        });
    },
    actions: {
        deleteIndex(index) {
            var route = this;
            localforage.getItem('playlist', function (err, playlist) {
                if (err != null) {
                    alert("ERROR: The webapp can't load cache database, use this app in storage compatible Code: " + err);
                } else if (playlist == null) {
                    alert("ERROR: A playlist is not created, use the menu to create a playlist");
                    route.transitionTo('index');
                } else {
                    playlist.splice(index, 1);
                    localforage.setItem('playlist', playlist, function (err2) {
                        if (err2 != null) {
                            alert("ERROR: The webapp can't cache database, use this app in storage compatible Code:" + err2);
                        } else {
                            route.refresh();
                        }
                    });
                }
            });
        },
        selectTheme(index, theme) {
            var route = this;
            localforage.getItem('playlist', function (err, playlist) {
                if (err != null) {
                    alert("ERROR: The webapp can't load cache database, use this app in storage compatible Code: " + err);
                } else if (playlist == null) {
                    alert("ERROR: A playlist is not created, use the menu to create a playlist");
                    route.transitionTo('index');
                } else {
                    playlist[index].ThemeIndex = theme;
                    localforage.setItem('playlist', playlist, function (err2) {
                        if (err2 != null) {
                            alert("ERROR: The webapp can't cache database, use this app in storage compatible Code:" + err2);
                        } else {
                            route.refresh();
                        }
                    });
                }
            });
        },
        randonize() {
            var route = this;
            localforage.getItem('playlist', function (err, playlist) {
                if (err != null) {
                    alert("ERROR: The webapp can't load cache database, use this app in storage compatible Code: " + err);
                } else if (playlist == null) {
                    alert("ERROR: A playlist is not created, use the menu to create a playlist");
                    route.transitionTo('index');
                } else {
                    var currentIndex = playlist.length, temporaryValue, randomIndex;
                    // While there remain elements to shuffle...
                    while (0 !== currentIndex) {
                        // Pick a remaining element...
                        randomIndex = Math.floor(Math.random() * currentIndex);
                        currentIndex -= 1;
                        // And swap it with the current element.
                        temporaryValue = playlist[currentIndex];
                        playlist[currentIndex] = playlist[randomIndex];
                        playlist[randomIndex] = temporaryValue;
                    }
                    localforage.setItem('playlist', playlist, function (err2) {
                        if (err2 != null) {
                            alert("ERROR: The webapp can't cache database, use this app in storage compatible Code:" + err2);
                        } else {
                            route.refresh();
                        }
                    });
                }
            });
        },
    }
});
