import Route from '@ember/routing/route';
import { Promise } from 'rsvp';
import { schedule } from '@ember/runloop';
import $ from 'jquery';

export default Route.extend({
    actions: {
        didTransition() {
            schedule('afterRender', this, function () {
                localforage.getItem('collectionTable', function (err, value) {
                    value.forEach(collection => {
                        window.$("#year_playlist_select").append($('<option value="' + collection.CollectionName + '">' + collection.CollectionName + '</option>'));
                    });
                    localforage.getItem('mappings', function (err, value) {
                        for (const collection in value) {
                            var year_Selected = $('#year_playlist_select').val();
                            if (collection == year_Selected) {
                                var seasons = value[collection];
                                for (const season in seasons) {
                                    window.$("#season_playlist_select").append($('<option value="' + season + '">' + season + '</option>'));
                                }
                            }
                        }
                    });
                });
                window.$("#year_playlist_select").change(function () {
                    $("#season_playlist_select").empty();
                    localforage.getItem('mappings', function (err, value) {
                        for (const collection in value) {
                            var year_Selected = $('#year_playlist_select').val();
                            if (collection == year_Selected) {
                                var seasons = value[collection];
                                for (const season in seasons) {
                                    window.$("#season_playlist_select").append($('<option value="' + season + '">' + season + '</option>'));
                                }
                            }
                        }
                    });
                });
            });
            return true;
        },
        startPlaylist() {
            var route = this;
            window.$('#playlist_wait').modal({backdrop: 'static', keyboard: false});

            var RegisterPlaylist = (function(slugs) {
                return new Promise(function(resolve) {
                    localforage.getItem('serieTable', function (err_anime, animes) {
                        var platplay = [];
                        slugs.forEach(slug => {
                            if (err_anime != null) {
                                alert("ERROR: The webapp can't load cache database, use this app in storage compatible Code:" + err_anime);
                            } else {
                                var anime = animes[slug];
                                if (anime.Themes.length > 0) {
                                  platplay.push({
                                      AnimeLink: anime.AnimeLink,
                                      Mappings: anime.Mappings,
                                      ThemeIndex: 0,
                                      Themes: anime.Themes,
                                  });
                              }
                            }
                        });
                        resolve(platplay);
                    });
                });
            });

            var ConvertList = (function(year, season) {
                localforage.getItem('mappings', function (err, value) {
                    var slugList = [];
                    var seasons = value[year];
                    seasons[season].forEach(slug => {
                        slugList.push(slug);
                    });
                    RegisterPlaylist(slugList).then(function(playlist) {
                        localforage.setItem('playlist', playlist, function(setErr) {
                            if (setErr != null) {
                                alert("ERROR: The webapp can't load cache database, use this app in storage compatible Code:" + setErr);
                            } else {
                                window.$("#playlist_wait").modal('hide');
                                route.transitionTo('playlist');
                            }
                        });
                    });
                });
            });

            ConvertList($("#year_playlist_select").val(), $("#season_playlist_select").val());
        }
    }
});
