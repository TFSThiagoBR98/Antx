import Route from '@ember/routing/route';
import $ from 'jquery';
import RSVP, { Promise } from 'rsvp';
import { schedule } from '@ember/runloop';

export default Route.extend({
    model(params) {
        var route = this;
        return new Promise(function(resolve) {
            localforage.getItem('playlist', function (err, playlist) {
                if (err != null) {
                    alert("ERROR: The webapp can't load cache database, use this app in storage compatible Code: " + err);
                } else if (playlist == null) {
                    alert("ERROR: A playlist is not created, use the menu to create a playlist");
                    route.transitionTo('index');
                } else {
                    var nextIndex = parseInt(params.index) + 1;
                    var laterIndex = parseInt(params.index) + 2;

                    var NextItem = playlist[nextIndex];
                    var LaterItem = playlist[laterIndex];

                    // Render 2º block of playlist: Next
                    if (NextItem == undefined) {
                        NextItem = {
                            AnimeLink: {
                                AnimeName: "",
                                SeasonName: "",
                                CollectionName: ""
                            },
                            Mappings: {},
                            ThemeIndex: 0,
                            Themes: [
                                {
                                    FullName: ""
                                }
                            ]
                        };
                    }

                    // Render 3º block of playlist: Later
                    if (LaterItem == undefined) {
                        LaterItem = {
                            AnimeLink: {
                                AnimeName: "",
                                SeasonName: "",
                                CollectionName: ""
                            },
                            Mappings: {},
                            ThemeIndex: 0,
                            Themes: [
                                {
                                    FullName: ""
                                }
                            ]
                        };
                    }

                    resolve(new RSVP.hash({
                        index: parseInt(params.index) + 1,
                        themeid: parseInt(params.themeid),
                        themeCount: playlist[parseInt(params.index)].Themes.length,
                        count: playlist.length,
                        playlist: playlist,
                        currentAnime: playlist[parseInt(params.index)],
                        nextAnime: NextItem,
                        laterAnime: LaterItem
                    }));
                }
            });
        });
    },

    actions: {
        willTransition(transition) {
            if (transition.to.name === 'playlist.play') {
                if (window.plyrInstance != null && window.plyrInstance.pip) {
                    localStorage.setItem('pip_player', true);
                    window.plyrInstance.pip = false;
                }
            } else {
                if (window.plyrInstance != null) {
                    window.plyrInstance.destroy();
                    window.plyrInstance = null;
                }
            }
        },
        downVideo() {
            window.open(window.plyrInstance.media.currentSrc, 'Download');
        },
        didTransition() {
            var classe = this;
            var model = classe.modelFor(classe.routeName);
            localStorage.setItem('playlist.index', model.index);

            schedule('afterRender', this, function () {
                // Player Loader
                var model = classe.modelFor(classe.routeName);

                let videoTracks = [];

                localforage.setItem('playlist.index', model.index);
                localforage.setItem('playlist.themeindex', model.themeid);

                model.currentAnime.Themes[model.themeid].Videos.forEach(element => {
                    videoTracks.push({
                        src: element.Link,
                        type: 'video/webm',
                        label: element.Name,
                        size: element.Quality
                    });
                });

                if (!window.plyrInstance) {
                    window.plyrInstance = new window.Plyr('#player', {
                        autoPlay: true
                    });
                    window.plyrInstance.elements.container.classList.add("player-playlist");
                    window.plyrInstance.on('ended', function() {
                        localforage.getItem('setting-repeat', function (err, setting) {
                            model = classe.modelFor(classe.routeName);
                            localStorage.setItem('pip_player', window.plyrInstance.pip);
                            let indexAnime = localStorage.getItem('playlist.index');
                            if (setting == null || setting == false) {
                                localforage.getItem('setting-allthemes', function (err, settingd) {
                                    if (settingd == null || settingd == false) {
                                        if (indexAnime < model.count) {
                                            classe.transitionTo(classe.routeName, indexAnime, model.currentAnime.ThemeIndex);
                                        }
                                    } else {
                                        if (model.themeid < 0 || model.themeid + 1 >= model.themeCount) {
                                            if (indexAnime < model.count) {
                                                classe.transitionTo(classe.routeName, indexAnime, model.currentAnime.ThemeIndex);
                                            }
                                        } else {
                                            classe.transitionTo(classe.routeName, indexAnime - 1, model.themeid + 1);
                                        }
                                    }

                                });
                            } else {
                                window.plyrInstance.restart();
                                window.plyrInstance.play();
                            }
                        });
                    });
                    window.plyrInstance.on('canplay', function() {
                        window.plyrInstance.pip = (localStorage.getItem('pip_player') === 'true');
                    });
                    window.plyrInstance.on('qualitychange', function() {
                        localStorage.setItem('pip_player', false);
                        window.plyrInstance.pip = false;
                    })
                } else {
                    if (window.plyrInstance != null) {
                        window.plyrInstance.pip = false;
                    }
                }

                window.plyrInstance.source = {
                    type: 'video',
                    title: classe.modelFor(classe.routeName).currentAnime.Themes[model.themeid].FullName,
                    sources: videoTracks
                }

                localforage.getItem('setting-allthemes', function (err, setting) {
                    if (setting == null || setting == false) {
                        $("#btn_allthemes").removeClass("btn-success").addClass("btn-danger");
                        $("#nextPlaying").text(model.nextAnime.Themes[model.nextAnime.ThemeIndex].FullName);
                        $("#nextPlaying_SerieName").text(model.nextAnime.AnimeLink.AnimeName);
                        $("#nextPlaying_Info").text(model.nextAnime.AnimeLink.SeasonName + " " + model.nextAnime.AnimeLink.CollectionName);
                        $("#laterPlaying").text(model.laterAnime.Themes[model.laterAnime.ThemeIndex].FullName);
                        $("#laterPlaying_SerieName").text(model.laterAnime.AnimeLink.AnimeName);
                        $("#laterPlaying_Info").text(model.laterAnime.AnimeLink.SeasonName + " " + model.laterAnime.AnimeLink.CollectionName);
                    } else {
                        if ($("#btn_allthemes").hasClass("btn-danger")) {
                            $("#btn_allthemes").removeClass("btn-danger").addClass("btn-success");
                        }

                        if (model.themeid < 0 || model.themeid + 1 == model.themeCount) {
                            $("#nextPlaying").text(model.nextAnime.Themes[model.nextAnime.ThemeIndex].FullName);
                            $("#nextPlaying_SerieName").text(model.nextAnime.AnimeLink.AnimeName);
                            $("#nextPlaying_Info").text(model.nextAnime.AnimeLink.SeasonName + " " + model.nextAnime.AnimeLink.CollectionName);
                        } else {
                            $("#nextPlaying").text(model.currentAnime.Themes[model.themeid + 1].FullName);
                            $("#nextPlaying_SerieName").text(model.currentAnime.AnimeLink.AnimeName);
                            $("#nextPlaying_Info").text(model.currentAnime.AnimeLink.SeasonName + " " + model.currentAnime.AnimeLink.CollectionName);
                        }

                        if (model.themeid < 0 || model.themeid + 2 == model.themeCount) {
                            $("#laterPlaying").text(model.nextAnime.Themes[model.nextAnime.ThemeIndex].FullName);
                            $("#laterPlaying_SerieName").text(model.nextAnime.AnimeLink.AnimeName);
                            $("#laterPlaying_Info").text(model.nextAnime.AnimeLink.SeasonName + " " + model.nextAnime.AnimeLink.CollectionName);
                        } if (model.themeid < 0 || model.themeid + 2 > model.themeCount) {
                            if (model.nextAnime.ThemeIndex + 1 >= model.nextAnime.Themes.length) {
                                $("#laterPlaying").text(model.laterAnime.Themes[model.laterAnime.ThemeIndex].FullName);
                                $("#laterPlaying_SerieName").text(model.laterAnime.AnimeLink.AnimeName);
                                $("#laterPlaying_Info").text(model.laterAnime.AnimeLink.SeasonName + " " + model.laterAnime.AnimeLink.CollectionName);
                            } else {
                                $("#laterPlaying").text(model.nextAnime.Themes[model.nextAnime.ThemeIndex + 1].FullName);
                                $("#laterPlaying_SerieName").text(model.nextAnime.AnimeLink.AnimeName);
                                $("#laterPlaying_Info").text(model.nextAnime.AnimeLink.SeasonName + " " + model.nextAnime.AnimeLink.CollectionName);
                            }
                        } else {
                            $("#laterPlaying").text(model.currentAnime.Themes[model.themeid + 2].FullName);
                            $("#laterPlaying_SerieName").text(model.currentAnime.AnimeLink.AnimeName);
                            $("#laterPlaying_Info").text(model.currentAnime.AnimeLink.SeasonName + " " + model.currentAnime.AnimeLink.CollectionName);
                        }
                    }
                });

                //RightBar Playlist Itens
                $("#currentPlaying").text(model.currentAnime.Themes[model.themeid].FullName);

                $("#topbar").text("Playing " +
                    model.index + "/" +
                    model.count +
                    " " + model.currentAnime.Themes[model.themeid].FullName + " - " +
                    model.currentAnime.AnimeLink.AnimeName + " (" +
                    model.currentAnime.AnimeLink.SeasonName + " " +
                    model.currentAnime.AnimeLink.CollectionName + ")"
                );
                if (model.currentAnime.Mappings.MyAnimeList != undefined) {
                    window.$("#btn_mal").removeClass("disabled").attr("href", "https://myanimelist.net/anime/" + model.currentAnime.Mappings.MyAnimeList);
                }
                if (model.currentAnime.Mappings.KitsuSlug != undefined) {
                    window.$("#btn_kitsu").removeClass("disabled").attr("href", "https://kitsu.io/anime/" + model.currentAnime.Mappings.KitsuSlug);
                }
                if (model.currentAnime.Mappings.Anilist != undefined) {
                    window.$("#btn_anilist").removeClass("disabled").attr("href", "https://anilist.co/anime/" + model.currentAnime.Mappings.Anilist);
                }

                if (model.index > 1) {
                    window.$("#btn_prev").removeClass("disabled");
                }

                if (model.index < model.count) {
                    window.$("#btn_next").removeClass("disabled");
                }

                if (model.themeid > 1) {
                    window.$("#btn_prevTheme").removeClass("disabled");
                }

                if (model.themeid < model.themeCount) {
                    window.$("#btn_nextTheme").removeClass("disabled");
                }

                localforage.getItem('setting-repeat', function (err, setting) {
                    if (setting == null || setting == false) {
                        if ($("#btn_repeat").hasClass("btn-danger")) {
                            $("#btn_repeat").removeClass("btn-danger").addClass("btn-success");
                        }
                    } else {
                        $("#btn_repeat").removeClass("btn-success").addClass("btn-danger");
                    }
                });

                localforage.getItem('setting-allthemes', function (err, setting) {
                    if (setting == null || setting == false) {
                        if ($("#btn_allthemes").hasClass("btn-danger")) {
                            $("#btn_allthemes").removeClass("btn-danger").addClass("btn-success");
                        }
                    } else {
                        $("#btn_allthemes").removeClass("btn-success").addClass("btn-danger");
                    }
                });
            });
            return true;
        },
        switchThemeOld(index) {
            var classe = this;

            let videoTracks = [];

            if (index < 0 || index > classe.modelFor(classe.routeName).currentAnime.Themes.count) {
                return;
            }

            classe.modelFor(classe.routeName).currentAnime.Themes[index].Videos.forEach(element => {
                videoTracks.push({
                    src: element.Link,
                    type: 'video/webm',
                    label: element.Name,
                    size: element.Quality
                });
            });

            if (window.plyrInstance != null) {
                localStorage.setItem('pip_player', window.plyrInstance.pip);
                window.plyrInstance.pip = false;
            }

            window.plyrInstance.source = {
                type: 'video',
                title: classe.modelFor(classe.routeName).currentAnime.Themes[index].FullName,
                sources: videoTracks
            };

            //RightBar Playlist Itens
            $("#currentPlaying").text(classe.modelFor(classe.routeName).currentAnime.Themes[index].FullName);

            $("#topbar").text("Playing " +
                classe.modelFor(classe.routeName).index + "/" +
                classe.modelFor(classe.routeName).count +
                " " + classe.modelFor(classe.routeName).currentAnime.Themes[index].FullName + " - " +
                classe.modelFor(classe.routeName).currentAnime.AnimeLink.AnimeName + " (" +
                classe.modelFor(classe.routeName).currentAnime.AnimeLink.SeasonName + " " +
                classe.modelFor(classe.routeName).currentAnime.AnimeLink.CollectionName + ")"
            );
            return true;
        },
        switchTheme(index) {
            var classe = this;
            if (index < 0 || index > classe.modelFor(classe.routeName).currentAnime.Themes.count) {
                return;
            }
            localStorage.setItem('pip_player', window.plyrInstance.pip);
            this.transitionTo(this.routeName, (classe.modelFor(classe.routeName).index - 1), index);
            return true;
        },
        prevAnime() {
            var classe = this;
            if ((classe.modelFor(classe.routeName).index - 2) < 0) {
                return;
            }
            localStorage.setItem('pip_player', window.plyrInstance.pip);
            localStorage.setItem('playlist.index', (classe.modelFor(classe.routeName).index - 2));
            this.transitionTo(this.routeName, (classe.modelFor(classe.routeName).index - 2), 0);
            return true;
        },
        nextAnime() {
            var classe = this;
            if (classe.modelFor(classe.routeName).index >= classe.modelFor(classe.routeName).count) {
                return;
            }
            localStorage.setItem('pip_player', window.plyrInstance.pip);
            localStorage.setItem('playlist.index', classe.modelFor(classe.routeName).index);
            this.transitionTo(this.routeName, classe.modelFor(classe.routeName).index, 0);
            return true;
        },
        prevTheme() {
            var classe = this;
            if ((classe.modelFor(classe.routeName).themeid) < 0) {
                return;
            }
            localStorage.setItem('pip_player', window.plyrInstance.pip);
            this.transitionTo(this.routeName, (classe.modelFor(classe.routeName).index - 1), (classe.modelFor(classe.routeName).themeid - 1));
            return true;
        },
        nextTheme() {
            var classe = this;
            if (classe.modelFor(classe.routeName).themeid + 1 >= classe.modelFor(classe.routeName).themeCount) {
                return;
            }
            localStorage.setItem('pip_player', window.plyrInstance.pip);
            this.transitionTo(this.routeName, (classe.modelFor(classe.routeName).index - 1), (classe.modelFor(classe.routeName).themeid + 1));
            return true;
        },
        randomPlaylist() {
            var route = this;
            localforage.getItem('playlist', function (err, playlist) {
                if (err != null) {
                    alert("ERROR: The webapp can't load cache database, use this app in storage compatible Code: " + err);
                } else if (playlist == null || playlist == []) {
                    alert("ERROR: A playlist is not created, use the menu to create a playlist");
                    route.transitionTo('index');
                } else {
                    localStorage.setItem('pip_player', window.plyrInstance.pip);
                    var index = Math.floor(Math.random() * playlist.length);
                    localStorage.setItem('playlist.index', index);
                    route.transitionTo(route.routeName, index, 0);
                }
            });
        },
        toggleRepeat() {
            localforage.getItem('setting-repeat', function (err, setting) {
                if (setting == null || setting == false) {
                    localforage.setItem('setting-repeat', true, function (err2) {
                        if (err2 != null) {
                            alert("ERROR: The webapp can't cache database, use this app in storage compatible Code:" + err2);
                        } else {
                            $("#btn_repeat").removeClass("btn-success").addClass("btn-danger");
                        }
                    });
                } else {
                    localforage.setItem('setting-repeat', false, function (err2) {
                        if (err2 != null) {
                            alert("ERROR: The webapp can't cache database, use this app in storage compatible Code:" + err2);
                        } else {
                            if ($("#btn_repeat").hasClass("btn-danger")) {
                                $("#btn_repeat").removeClass("btn-danger").addClass("btn-success");
                            }
                        }
                    });
                }
            });
        },
        togglePassAllThemes() {
            localforage.getItem('setting-allthemes', function (err, setting) {
                if (setting == null || setting == false) {
                    localforage.setItem('setting-allthemes', true, function (err2) {
                        if (err2 != null) {
                            alert("ERROR: The webapp can't cache database, use this app in storage compatible Code:" + err2);
                        } else {
                            $("#btn_allthemes").removeClass("btn-success").addClass("btn-danger");
                        }
                    });
                } else {
                    localforage.setItem('setting-allthemes', false, function (err2) {
                        if (err2 != null) {
                            alert("ERROR: The webapp can't cache database, use this app in storage compatible Code:" + err2);
                        } else {
                            if ($("#btn_allthemes").hasClass("btn-danger")) {
                                $("#btn_allthemes").removeClass("btn-danger").addClass("btn-success");
                            }
                        }
                    });
                }
            });
        }
    }
});
