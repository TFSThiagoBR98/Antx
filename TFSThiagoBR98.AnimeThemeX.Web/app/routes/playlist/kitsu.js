import Route from '@ember/routing/route';
import $ from 'jquery';

export default Route.extend({
    actions: {
        startPlaylist() {
            var route = this;
            window.$('#playlist_wait').modal({backdrop: 'static', keyboard: false});
            var userfield = $("#kitsuuser");
            var filter_waching = $("#st_what");
            var filter_drop = $("#st_drop");
            var filter_hold = $("#st_hold");
            var filter_complete = $("#st_com");
            var filter_plwh = $("#st_plwh");

            if (userfield.val() == "") {
                window.$("#playlist_wait").modal('hide');
                alert("Fill the username")
                return;
            }
            var filters = [];

            if (filter_waching.is(":checked")) {
                filters.push('current');
            }

            if (filter_drop.is(":checked")) {
                filters.push('dropped');
            }

            if (filter_hold.is(":checked")) {
                filters.push('on_hold');
            }

            if (filter_complete.is(":checked")) {
                filters.push('completed');
            }

            if (filter_plwh.is(":checked")) {
                filters.push('planned');
            }

            if (!filter_plwh.is(":checked")
              && !filter_complete.is(":checked")
              && !filter_hold.is(":checked")
              && !filter_drop.is(":checked")
              && !filter_waching.is(":checked")) {
                alert("At least one checkbox must be checked")
                return;
            }

            var RegisterPlaylist = (function(slugs) {
                return new Promise(function(resolve) {
                    localforage.getItem('serieTable', function (err_anime, animes) {
                        var platplay = [];
                        slugs.forEach(slug => {
                            if (err_anime != null) {
                                alert("ERROR: The webapp can't load cache database, use this app in storage compatible Code:" + err_anime);
                            } else {
                                var anime = animes[slug];
                                if (anime.Themes.length > 0) {
                                  platplay.push({
                                      AnimeLink: anime.AnimeLink,
                                      Mappings: anime.Mappings,
                                      ThemeIndex: 0,
                                      Themes: anime.Themes,
                                  });
                              }
                            }
                        });
                        resolve(platplay);
                    });
                });
            });

            var ConvertList = (function(ids) {
                localforage.getItem('kitsuTable', function (err, value) {
                    var slugList = [];
                    ids.forEach(id => {
                        if (value[id] != undefined) {
                            slugList.push(value[id]);
                        }
                    });
                    RegisterPlaylist(slugList).then(function(playlist) {
                        localforage.setItem('playlist', playlist, function(setErr) {
                            if (setErr != null) {
                                alert("ERROR: The webapp can't load cache database, use this app in storage compatible Code:" + setErr);
                            } else {
                                window.$("#playlist_wait").modal('hide');
                                route.transitionTo('playlist');
                            }
                        });
                    });
                });
            });

            var UserLoad = (function(userId, filter) {
                $.getJSON("https://kitsu.io/api/edge/library-entries?fields[anime]=id&filter[user_id]=" + userId + "&filter[kind]=anime&filter[status]=" + filter + "&include=anime&page[offset]=0&page[limit]=400&sort=status%2C-progressed_at", function(data) {
                    var list = [];
                    $.each(data.data, function(key, val) {
                        list.push(val.relationships.anime.data.id);
                    });
                    ConvertList(list);
                });
            });

            $.getJSON('https://kitsu.io/api/edge/users?filter[slug]=' + userfield.val(), function(data) {
                UserLoad(data.data[0].id, filters.join(","));
            });
        }
    }
});
