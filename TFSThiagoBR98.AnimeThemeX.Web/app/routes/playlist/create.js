import Route from '@ember/routing/route';

export default Route.extend({
    RegisterPlaylist: (function (slugs) {
        return new Promise(function (resolve) {
            localforage.getItem('serieTable', function (err_anime, animes) {
                var playback = [];
                slugs.forEach(slug => {
                    if (err_anime != null) {
                        alert("ERROR: Failed to get item from local storage - Code:" + err_anime);
                    } else {
                        var anime = animes[slug];
                        if (anime.Themes.length > 0) {
                            playback.push({
                                AnimeLink: anime.AnimeLink,
                                Mappings: anime.Mappings,
                                ThemeIndex: 0,
                                Themes: anime.Themes,
                            });
                        }
                    }
                });
                resolve(playback);
            });
        });
    }),
    actions: {
        startAnilist() {
            window.$('#playlist_wait').modal({ backdrop: 'static', keyboard: false });
            var route = this;
            var userfield = window.$("#anilist_user");
            var filter_waching = window.$("#anilist_current");
            var filter_drop = window.$("#anilist_dropped");
            var filter_hold = window.$("#anilist_paused");
            var filter_complete = window.$("#anilist_complete");
            var filter_plwh = window.$("#anilist_planned");

            if (userfield.val() == "") {
                window.$("#playlist_wait").modal('hide');
                userfield.addClass("is-invalid");
                return;
            }
            var filters = [];

            if (filter_waching.is(":checked")) {
                filters.push('CURRENT');
            }

            if (filter_drop.is(":checked")) {
                filters.push('DROPPED');
            }

            if (filter_hold.is(":checked")) {
                filters.push('PAUSED');
            }

            if (filter_complete.is(":checked")) {
                filters.push('COMPLETED');
            }

            if (filter_plwh.is(":checked")) {
                filters.push('PLANNING');
            }

            if (!filter_plwh.is(":checked")
                && !filter_complete.is(":checked")
                && !filter_hold.is(":checked")
                && !filter_drop.is(":checked")
                && !filter_waching.is(":checked")) {
                alert("At least one checkbox must be checked")
                return;
            }

            var ConvertList = (function (collections) {
                localforage.getItem('anilistTable', function (err, value) {
                    var slugList = [];
                    collections.forEach(collection => {
                        if (filters.includes(collection.status)) {
                            collection.entries.forEach(id => {
                                if (value[id.mediaId] != undefined) {
                                    slugList.push(value[id.mediaId]);
                                }
                            });
                        }
                    });
                    route.RegisterPlaylist(slugList).then(function (playlist) {
                        localforage.setItem('playlist', playlist, function (setErr) {
                            if (setErr != null) {
                                alert("ERROR: Failed to set item in the local storage Code:" + setErr);
                            } else {
                                window.$("#playlist_wait").modal('hide');
                                route.transitionTo('playlist');
                            }
                        });
                    });
                });
            });

            var query = `
            query($user: String) {
                MediaListCollection(userName: $user, type: ANIME) {
                    lists {
                        entries {
                            mediaId
                        }
                        status
                    }
                }
            }
            `;

            var variables = {
                user: userfield.val()
            };

            var url = 'https://graphql.anilist.co',
                options = {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        'Accept': 'application/json',
                    },
                    body: JSON.stringify({
                        query: query,
                        variables: variables
                    })
                };

            var handleResponse = (function (response) {
                return response.json().then(function (json) {
                    return response.ok ? json : Promise.reject(json);
                });
            });

            var handleData = (function (data) {
                ConvertList(data.data.MediaListCollection.lists);
            });

            var handleError = (function (error) {
                alert(error);
            });


            fetch(url, options).then(handleResponse).then(handleData).catch(handleError);
        }
    }
});
