import Route from '@ember/routing/route';
import { Promise } from 'rsvp';
import { schedule } from '@ember/runloop';
import $ from 'jquery';

export default Route.extend({
    actions: {
        didTransition() {
            schedule('afterRender', this, function () {
                localforage.getItem('serieMappings', function (err, value) {
                    $("#nums").attr("max", value.length);
                    $("#nums").val();
                });
            });
            return true;
        },
        startPlaylist() {
            var route = this;
            var count = $("#nums").val();
            if (count == null || count == undefined || count < 3) {
                alert("Set a valid value for Num");
                return;
            }
            window.$('#playlist_wait').modal({backdrop: 'static', keyboard: false});
            var Random = (function (arr, n) {
                var currentIndex = arr.length, temporaryValue, randomIndex;
                // While there remain elements to shuffle...
                while (0 !== currentIndex) {
                    // Pick a remaining element...
                    randomIndex = Math.floor(Math.random() * currentIndex);
                    currentIndex -= 1;
                    // And swap it with the current element.
                    temporaryValue = arr[currentIndex];
                    arr[currentIndex] = arr[randomIndex];
                    arr[randomIndex] = temporaryValue;
                }
                arr.length = n;
                return arr;
            });
            var RegisterPlaylist = (function(slugs) {
                return new Promise(function(resolve) {
                    localforage.getItem('serieTable', function (err_anime, animes) {
                        var platplay = [];
                        slugs.forEach(slug => {
                            if (err_anime != null) {
                                alert("ERROR: The webapp can't load cache database, use this app in storage compatible Code:" + err_anime);
                            } else {
                                var anime = animes[slug];
                                if (anime.Themes.length > 0) {
                                  platplay.push({
                                      AnimeLink: anime.AnimeLink,
                                      Mappings: anime.Mappings,
                                      ThemeIndex: 0,
                                      Themes: anime.Themes,
                                  });
                              }
                            }
                        });
                        resolve(platplay);
                    });
                });
            });

            var ConvertList = (function() {
                localforage.getItem('serieMappings', function (err, value) {
                    var slugList = Random(value, $("#nums").val());
                    RegisterPlaylist(slugList).then(function(playlist) {
                        localforage.setItem('playlist', playlist, function(setErr) {
                            if (setErr != null) {
                                alert("ERROR: The webapp can't load cache database, use this app in storage compatible Code:" + setErr);
                            } else {
                                window.$("#playlist_wait").modal('hide');
                                route.transitionTo('playlist');
                            }
                        });
                    });
                });
            });

            ConvertList();
        }
    }
});
