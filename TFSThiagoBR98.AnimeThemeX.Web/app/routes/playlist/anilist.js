import Route from '@ember/routing/route';
import $ from 'jquery';

export default Route.extend({
    actions: {
        startPlaylist() {
            var route = this;
            window.$('#playlist_wait').modal({backdrop: 'static', keyboard: false});
            var userfield = $("#anilistuser");
            var filter_waching = $("#st_what");
            var filter_drop = $("#st_drop");
            var filter_hold = $("#st_hold");
            var filter_complete = $("#st_com");
            var filter_plwh = $("#st_plwh");

            if (userfield.val() == "") {
                window.$("#playlist_wait").modal('hide');
                alert("Fill the username")
                return;
            }
            var filters = [];

            if (filter_waching.is(":checked")) {
                filters.push('CURRENT');
            }

            if (filter_drop.is(":checked")) {
                filters.push('DROPPED');
            }

            if (filter_hold.is(":checked")) {
                filters.push('PAUSED');
            }

            if (filter_complete.is(":checked")) {
                filters.push('COMPLETED');
            }

            if (filter_plwh.is(":checked")) {
                filters.push('PLANNING');
            }

            if (!filter_plwh.is(":checked")
              && !filter_complete.is(":checked")
              && !filter_hold.is(":checked")
              && !filter_drop.is(":checked")
              && !filter_waching.is(":checked")) {
                alert("At least one checkbox must be checked")
                return;
            }

            var RegisterPlaylist = (function(slugs) {
                return new Promise(function(resolve) {
                    localforage.getItem('serieTable', function (err_anime, animes) {
                        var platplay = [];
                        //slugs.sort();
                        slugs.forEach(slug => {
                            if (err_anime != null) {
                                alert("ERROR: The webapp can't load cache database, use this app in storage compatible Code:" + err_anime);
                            } else {
                                var anime = animes[slug];
                                if (anime.Themes.length > 0) {
                                    platplay.push({
                                        AnimeLink: anime.AnimeLink,
                                        Mappings: anime.Mappings,
                                        ThemeIndex: 0,
                                        Themes: anime.Themes,
                                    });
                                }
                            }
                        });
                        resolve(platplay);
                    });
                });
            });

            var ConvertList = (function(collections) {
                localforage.getItem('anilistTable', function (err, value) {
                    var slugList = [];
                    var listData = [];
                    collections.forEach(collection => {
                        if (filters.includes(collection.status)) {
                            collection.entries.forEach(id => {
                                if (value[id.mediaId] != undefined) {
                                    listData.push(id);
                                    //slugList.push(value[id.mediaId]);
                                }
                            });
                        }
                    });
                    listData.sort(function (a, b) {
                        if (a.media.startDate.year < b.media.startDate.year) {
                            return 1;
                        }
                        if (a.media.startDate.year > b.media.startDate.year) {
                            return -1;
                        }
                        if (a.media.startDate.month < b.media.startDate.month) {
                            return 1;
                        }
                        if (a.media.startDate.month > b.media.startDate.month) {
                            return -1;
                        }
                        if (a.media.startDate.day < b.media.startDate.day) {
                            return 1;
                        }
                        if (a.media.startDate.day > b.media.startDate.day) {
                            return -1;
                        }
                        return 0;
                    });
                    listData.forEach(anime => {
                        slugList.push(value[anime.mediaId]);
                    })
                    RegisterPlaylist(slugList).then(function(playlist) {
                        localforage.setItem('playlist', playlist, function(setErr) {
                            if (setErr != null) {
                                alert("ERROR: The webapp can't load cache database, use this app in storage compatible Code:" + setErr);
                            } else {
                                window.$("#playlist_wait").modal('hide');
                                route.transitionTo('playlist');
                            }
                        });
                    });
                });
            });

            var query = `
            query($user: String) {
                MediaListCollection(userName: $user, type: ANIME, sort: UPDATED_TIME_DESC) {
                    lists {
                        entries {
                            mediaId
                            media {
                                startDate{year month day}
                            }
                        }
                        status
                    }
                }
            }
            `;

            var variables = {
                user: userfield.val()
            };

            var url = 'https://graphql.anilist.co',
            options = {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                },
                body: JSON.stringify({
                    query: query,
                    variables: variables
                })
            };

            var handleResponse = (function (response) {
                return response.json().then(function (json) {
                    return response.ok ? json : Promise.reject(json);
                });
            });

            var handleData = (function (data) {
                ConvertList(data.data.MediaListCollection.lists);
            });

            var handleError = (function (error) {
                alert(error);
            });


            fetch(url, options).then(handleResponse).then(handleData).catch(handleError);
        }
    }
});
