import Route from '@ember/routing/route';

export default Route.extend({
    actions: {
        startPlaylist() {
            var route = this;
            window.$('#playlist_wait').modal({backdrop: 'static', keyboard: false}); 
            var RegisterPlaylist = (function() {
                return new Promise(function(resolve) {
                    localforage.getItem('rankPopTable', function(err, rank) {
                        localforage.getItem('serieTable', function (err_anime, animes) {
                            var platplay = [];
                            rank.forEach(slug => {
                                if (err_anime != null) {
                                    alert("ERROR: The webapp can't load cache database, use this app in storage compatible Code:" + err_anime);
                                } else {
                                    var anime = animes[slug.Anime.AnimeID];
                                    platplay.push({
                                        AnimeLink: anime.AnimeLink,
                                        Mappings: anime.Mappings,
                                        ThemeIndex: 0,
                                        Themes: anime.Themes,
                                    });
                                }
                            });
                            resolve(platplay);
                        });
                    });
                });
            });

            RegisterPlaylist().then(function(playlist) {
                localforage.setItem('playlist', playlist, function(setErr) {
                    if (setErr != null) {
                        alert("ERROR: The webapp can't load cache database, use this app in storage compatible Code:" + setErr);
                    } else {
                        window.$("#playlist_wait").modal('hide');
                        route.transitionTo('playlist');
                    }
                });
            });
        }
    }
});
