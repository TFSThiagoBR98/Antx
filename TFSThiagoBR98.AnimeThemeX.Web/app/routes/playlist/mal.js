import Route from '@ember/routing/route';
import $ from 'jquery';

export default Route.extend({
    actions: {
        startPlaylist() {
            var route = this;
            window.$('#playlist_wait').modal({backdrop: 'static', keyboard: false});
            var userfield = $("#maluser");
            var filter_waching = $("#st_what");
            var filter_drop = $("#st_drop");
            var filter_hold = $("#st_hold");
            var filter_complete = $("#st_com");
            var filter_plwh = $("#st_plwh");

            if (userfield.val() == "") {
                window.$("#playlist_wait").modal('hide');
            }
            var filters = [];

            if (filter_waching.is(":checked")) {
                filters.push(1);
            }

            if (filter_drop.is(":checked")) {
                filters.push(4);
            }

            if (filter_hold.is(":checked")) {
                filters.push(3);
            }

            if (filter_complete.is(":checked")) {
                filters.push(2);
            }

            if (filter_plwh.is(":checked")) {
                filters.push(6);
            }

            if (!filter_plwh.is(":checked")
              && !filter_complete.is(":checked")
              && !filter_hold.is(":checked")
              && !filter_drop.is(":checked")
              && !filter_waching.is(":checked")) {
                alert("At least one checkbox must be checked")
                return;
            }

            var RegisterPlaylist = (function(slugs) {
                return new Promise(function(resolve) {
                    localforage.getItem('serieTable', function (err_anime, animes) {
                        var platplay = [];
                        slugs.forEach(slug => {
                            if (err_anime != null) {
                                alert("ERROR: The webapp can't load cache database, use this app in storage compatible Code:" + err_anime);
                            } else {
                                var anime = animes[slug];
                                if (anime.Themes.length > 0) {
                                  platplay.push({
                                      AnimeLink: anime.AnimeLink,
                                      Mappings: anime.Mappings,
                                      ThemeIndex: 0,
                                      Themes: anime.Themes,
                                  });
                              }
                            }
                        });
                        resolve(platplay);
                    });
                });
            });

            var ConvertList = (function(ids) {
                localforage.getItem('malTable', function (err, value) {
                    var slugList = [];
                    ids.forEach(id => {
                        if (value[id] != undefined) {
                            slugList.push(value[id]);
                        }
                    });
                    RegisterPlaylist(slugList).then(function(playlist) {
                        localforage.setItem('playlist', playlist, function(setErr) {
                            if (setErr != null) {
                                alert("ERROR: Local Storage do not have data, Code:" + setErr);
                            } else {
                                window.$("#playlist_wait").modal('hide');
                                route.transitionTo('playlist');
                            }
                        });
                    });
                });
            });

            // https://cors-anywhere.herokuapp.com/
            $.getJSON("https://cors-anywhere.herokuapp.com/https://myanimelist.net/animelist/" + userfield.val() + "/load.json?status=7&offset=0",
                function(json) {
                    var animeList = [];
                    $.each(json, function(key, val) {
                        if (filters.includes(parseInt(val.status))) {
                            animeList.push(val.anime_id);
                        }
                    });
                    ConvertList(animeList);
                }
            );
        }
    }
});
