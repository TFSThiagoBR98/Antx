import Route from '@ember/routing/route';
import RSVP, { Promise } from 'rsvp';

export default Route.extend({
    model(params) {
        return new Promise(function(resolve) {
            localforage.getItem('searchTable', function (err, value) {
                if (err != null) {
                    alert("ERROR: The webapp can't load cache database, use this app in storage compatible Code:" + err);
                } else {
                    var founds = [];
                    for (const anime in value) {
                        if (value[anime].AnimeName.toLowerCase().includes(params.searchquery.toLowerCase())) {
                            founds.push({name: value[anime].AnimeName, poster: value[anime].PosterLink, animeid: value[anime].AnimeID});
                        }
                    }
                    resolve(new RSVP.hash({
                        results: founds,
                        queryString: params.searchquery
                    }));
                }
            });
        });
    }

});
