import Route from '@ember/routing/route';
import { Promise } from 'rsvp';

export default Route.extend({
    model(){
        return new Promise(function(resolve) {
            localforage.getItem('collectionTable', function (err, value) {
                if (err != null) {
                    alert("ERROR: The webapp can't load cache database, use this app in storage compatible Code:" + err);
                } else {
                    resolve(value);
                }
            });
            /*$.ajax({
                url:"/export/collections.json"
            }).then(response=>{
                resolve(response);
            });*/
        });
    }
});
