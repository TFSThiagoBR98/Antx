import Route from '@ember/routing/route';
import { Promise } from 'rsvp';
import { schedule } from '@ember/runloop';

export default Route.extend({
    model(params) {
        return new Promise(function(resolve) {
            localforage.getItem('serieTable', function (err, value) {
                if (err != null) {
                    alert("ERROR: The webapp can't load cache database, use this app in storage compatible Code:" + err);
                } else {
                    var entry = value[params.animeid];
                    resolve(entry);
                }
            });
        });
    },
    actions: {
        didTransition() {
            var value = this.modelFor(this.routeName);
            schedule('afterRender', this, function () {
                if (value.Mappings.MyAnimeList != undefined) {
                    window.$("#btn_mal").removeClass("disabled").attr("href", "https://myanimelist.net/anime/" + value.Mappings.MyAnimeList);
                }
                if (value.Mappings.KitsuSlug != undefined) {
                    window.$("#btn_kitsu").removeClass("disabled").attr("href", "https://kitsu.io/anime/" + value.Mappings.KitsuSlug);
                }
                if (value.Mappings.Anilist != undefined) {
                    window.$("#btn_anilist").removeClass("disabled").attr("href", "https://anilist.co/anime/" + value.Mappings.Anilist);
                }
            });
            if (value.Mappings.MyAnimeList != undefined) {
                window.$("#btn_mal").removeClass("disabled").attr("href", "https://myanimelist.net/anime/" + value.Mappings.MyAnimeList);
            }
            if (value.Mappings.KitsuSlug != undefined) {
                window.$("#btn_kitsu").removeClass("disabled").attr("href", "https://kitsu.io/anime/" + value.Mappings.KitsuSlug);
            }
            if (value.Mappings.Anilist != undefined) {
                window.$("#btn_anilist").removeClass("disabled").attr("href", "https://anilist.co/anime/" + value.Mappings.Anilist);
            }
        }
    }
});
