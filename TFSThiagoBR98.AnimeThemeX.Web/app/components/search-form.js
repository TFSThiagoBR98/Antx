import Component from '@ember/component';
import $ from 'jquery';
import { inject } from '@ember/service';

export default Component.extend({
    router: inject(),
    actions: {
        searcher: function() {
            if ($('#query').val().length < 3) {
                alert("Type at least 3 characters");
            } else {
                this.get('router').transitionTo('search', $('#query').val());
                //location.reload();
            }
        },
        insertNewline: function() {
            $('#searchSubmit').trigger("click");
        }
    }
});
