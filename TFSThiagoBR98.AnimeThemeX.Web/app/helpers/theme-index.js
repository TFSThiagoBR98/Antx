import { helper } from '@ember/component/helper';

export function themeIndex(params) {
    let [context, index] = params;
    if (context[index] != undefined) {
      return context[index].FullName;
    } else {
      return "";
    }
}

export default helper(themeIndex);
