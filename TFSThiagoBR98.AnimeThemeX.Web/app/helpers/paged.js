import { helper } from '@ember/component/helper';

export function paged(params) {
    let [page, perPage, index] = params;
    return ((page - 1) * perPage) + index;
}

export default helper(paged);
