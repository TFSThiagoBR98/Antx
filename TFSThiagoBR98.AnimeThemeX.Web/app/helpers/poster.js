import { helper } from '@ember/component/helper';

export function poster(params) {
    let [malId] = params;
    var pro = new Promise(function(resolve) {
        window.$.getJSON("https://api.jikan.moe/v3/anime/" + malId, function(data) {
            resolve(data.image_url);
        }).fail(function() {
            resolve("https://dummyimage.com/225x319/000000/fff");
        });
    });

    return pro;
}

export default helper(poster);
