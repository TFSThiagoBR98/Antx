import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | anime/theme', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:anime/theme');
    assert.ok(route);
  });
});
