import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | catalog/season', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:catalog/season');
    assert.ok(route);
  });
});
