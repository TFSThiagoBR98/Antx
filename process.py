## This script generate Stage II Json and Maps

import json
from datetime import datetime
import requests
import time

from pathlib import Path

output = Path("./ResultData")
output.mkdir(parents=True, exist_ok=True)

data = []

searchMap = {}

malMap = {}
anilistMap = {}
kitsuMap = {}

animeMap = {}

listCollection = {}
listAnime = []

## 1: Fetch Collection list
print('Starting...')
print('Fetch Collections....')
yearCollectionUrl = "https://api.animethemes.moe/animeyear/"
response = requests.request("GET", yearCollectionUrl)
collections = response.json()
time.sleep(5)
print(collections)
for collection in collections:
    listCollection[collection] = {}
    out = {
        'CollectionName': str(collection),
        'Seasons': {},
    }

    url = "https://api.animethemes.moe/animeyear/{}".format(collection)
    querystring = {
        "fields[anime]":"name,id,season,slug",
        "fields[resource]":"link,site,external_id",
        "fields[image]":"link,facet",
        "fields[video]":"link,nc,subbed,lyrics,uncen,source,overlap,resolution,id,basename",
        "fields[animethemeentry]":"version,episodes,nsfw,spoiler,notes,id",
        "fields[song]":"title",
        "fields[series]":"name,id",
        "include":"resources,images,series,animethemes,animethemes.animethemeentries,animethemes.animethemeentries.videos,animethemes.song"
    }
    response = requests.request("GET", url, params=querystring)
    seasons = response.json()
    print(str(collection))
    for season in seasons:
        listCollection[collection][season.capitalize()] = []
        sea = {
            'SeasonName': '{} {} Season'.format(collection, season.capitalize()),
            'SeasonType': season.capitalize(),
            'Animes': {}
        }
        print(season.capitalize())
        print(seasons[season])
        for anime in seasons[season]:
            id = anime.get('slug', anime.get('id'))
            ani = {
                'AnimeID': id,
                'ReleaseDate': '0001-01-01T00:00:00',
                'Mappings': {},
                'AnimeLink': {},
                'PosterLink': '',
                'CurrentAiring': False,
                'Themes': [],
            }
            sch = {
                'CollectionName': collection,
                'Season': 0,
                'SeasonName': season.capitalize(),
                'AnimeID': id,
                'AnimeName': anime['name'],
                'PosterLink': ''
            }
            completeTitle = "{} ({} {})".format(anime['name'], season.capitalize(), collection)

            ani['AnimeLink'] = sch
            searchMap[completeTitle] = sch

            print(anime['name'])
            for image in anime['images']:
                ani['PosterLink'] = image['link']
                ani['AnimeLink']['PosterLink'] = image['link']
                break
            
            for resource in anime['resources']:
                if resource['site'] == 'MyAnimeList':
                    ani['Mappings']['MyAnimeList'] = resource['external_id']
                    malMap[ani['Mappings']['MyAnimeList']] = id
                elif resource['site'] == 'AniList':
                    ani['Mappings']['Anilist'] = resource['external_id']
                    anilistMap[ani['Mappings']['Anilist']] = id
                elif resource['site'] == 'Kitsu':
                    ani['Mappings']['KitsuID'] = resource['external_id']
                    ani['Mappings']['KitsuSlug'] = resource['link']
                    kitsuMap[ani['Mappings']['KitsuID']] = id

            for animetheme in anime['animethemes']:
                for atentry in animetheme['animethemeentries']:
                    title = 'Unknown' if animetheme.get('song') == None or animetheme['song'].get('title') == None else animetheme['song']['title']
                    prefix = '{}{} V{}'.format(animetheme.get('type'), animetheme.get('sequence', 1) or 1, atentry.get('version', 1) or 1)
                    tname = '{} "{}"'.format(prefix, title)
                    the = {
                        'ThemeType': animetheme.get('type'),
                        'ThemeId': 't_{}_e_{}'.format(animetheme.get('id'), atentry.get('id')),
                        'ThemePrefix': prefix,
                        'FullName': tname,
                        'SongName': title,
                        'Number': animetheme.get('sequence', 1) or 1,
                        'Version': atentry.get('version', 1) or 1,
                        'Episodes': atentry.get('episodes'),
                        'Notes': '',
                        'IsNSFW': atentry.get('nsfw'),
                        'IsSpoiler': atentry.get('spoiler'),
                        'Videos': [],
                    }
                    for video in atentry['videos']:
                        vid = {
                            'VideoID': video.get('id'),
                            'Name': "{}p {}".format(video.get('resolution') or '720', video.get('source', 'UNK/TV') or 'UNK/TV'),
                            'Quality': video.get('resolution'),
                            'IsNC': video.get('nc'),
                            'IsLyrics': video.get('lyrics'),
                            'Source': video.get('source', 'UNK/TV'),
                            'Link': video.get('link'),
                            'Notes': ''
                        }
                        the['Videos'].append(vid)
                    ani['Themes'].append(the)
            sea['Animes'][id] = ani
            listCollection[collection][season.capitalize()].append(id)
            listAnime.append(id)
            animeMap[id] = ani
        out['Seasons'][season.capitalize()] = sea
    data.append(out)
    time.sleep(4)

version = {
    'GeneratedTime': str(datetime.now()),
    'AssemblyVersion': '3.0.0'
}
versionFile = output / 'Version.json'
with versionFile.open("w", encoding ="utf-8") as f:
    json.dump(version, f)

stageIIFile = output / 'StageII.json'
with stageIIFile.open("w", encoding ="utf-8") as f:
    json.dump(data, f)

anilistMapFile = output / 'Anilist.Map.json'
with anilistMapFile.open("w", encoding ="utf-8") as f:
    json.dump(anilistMap, f)

kitsuMapFile = output / 'Kitsu.Map.json'
with kitsuMapFile.open("w", encoding ="utf-8") as f:
    json.dump(kitsuMap, f)

malMapFile = output / 'MyAnimeList.Map.json'
with malMapFile.open("w", encoding ="utf-8") as f:
    json.dump(malMap, f)

animeMapFile = output / 'Anime.Map.json'
with animeMapFile.open("w", encoding ="utf-8") as f:
    json.dump(animeMap, f)

listAnimeFile = output / 'List.Anime.json'
with listAnimeFile.open("w", encoding ="utf-8") as f:
    json.dump(listAnime, f)

listCollectionsFile = output / 'List.Collections.json'
with listCollectionsFile.open("w", encoding ="utf-8") as f:
    json.dump(listCollection, f)

searchMapFile = output / 'Search.Map.json'
with searchMapFile.open("w", encoding ="utf-8") as f:
    json.dump(searchMap, f)

## Generate Top list

def generateRankTier(rank="bypopularity"):
    global malMap
    global animeMap

    rankUrl = "https://api.jikan.moe/v4/top/anime"
    rankCount = 1
    rankLimit = 150

    listAnimeRank = []

    currentPage = 1
    while (rankCount < rankLimit):
        querystring = {
            "filter": rank,
            "page": currentPage,
            "limit":"25"
        }
        response = requests.request("GET", rankUrl, params=querystring)
        jcol = response.json()

        for ent in jcol['data']:
            malId = ent['mal_id']
            print(ent['title'])
            malMapEntry = malMap.get(malId)
            if malMapEntry != None:
                ane = {
                    'Rank': rankCount,
                    'Anime': animeMap[malMapEntry]['AnimeLink']
                }
                listAnimeRank.append(ane)
            rankCount = rankCount + 1
        currentPage = currentPage + 1
        time.sleep(4)

    return listAnimeRank

print('Generate Rankings')

topPopular = generateRankTier(rank="bypopularity")
topPopularFile = output / 'Top.Popularity.json'
with topPopularFile.open("w", encoding ="utf-8") as f:
    json.dump(topPopular, f)

topAiring = generateRankTier(rank="airing")
topAiringFile = output / 'Top.Airing.json'
with topAiringFile.open("w", encoding ="utf-8") as f:
    json.dump(topAiring, f)

topScore = generateRankTier(rank="favorite")
topScoreFile = output / 'Top.Score.json'
with topScoreFile.open("w", encoding ="utf-8") as f:
    json.dump(topScore, f)