AnimeThemeX - /r/AnimeThemes Wiki Parser and https://animethemex.tk Website
=============

# Project Structure

- TFSThiagoBR98.AnimeThemeX - Main App
- TFSThiagoBR98.AnimeThemeX.Health - Health Check Library
- TFSThiagoBR98.AnimeThemeX.Library - Parser Library
- TFSThiagoBR98.AnimeThemeX.Web - [animethemex.tk](https://animethemex.tk) Ember WebApp

# Main App

## Dependencies

- [.NET Core SDK v2.1 or greater.](https://www.microsoft.com/net/download)

## Execute
- (Optional) Extract Cache.tar.lzma to ./ResultData
- Run dotnet run -c Release -p ./TFSThiagoBR98.AnimeThemeX/TFSThiagoBR98.AnimeThemeX.csproj scan

## Arguments

### scan Mode - Dump /r/AnimeThemes Wiki to Json Files in ./ResultData

- --skipmyanimelist - Don't get data on MyAnimeList
- --skipkitsu - Don't get data on Kitsu
- --skipanilist - Don't get data on Anilist
- --disablecache - Ignore Cache
- --removecache - Remove All Cache from Cache Directory
- --nowait - Not Sleep between HTTP requests
- --waitime=ms - Set Sleep Time between HTTP requests - Default 3000

### health Mode - Check Status for Video Links on Json Dump

**RUN scan First**

- --skiphttpcheck - Don't check http status
- --skipmarkdowngen - Don't gen Markdown Report (Explicit --SkipHtmlGen)
- --skiphtmlgen - Don't gen HTML Report
- --removecheckfiles - Delete all ffprobe reports
- --timeoutlimit=ms - HTTP Check TimeOut

# Web App

On https://animethemex.tk

## Features

- Playlist Support (MyAnimeList(Api Disabled), Kitsu, Anilist, By Season, By Year, By Popularity, By Score, By Airing Score, Random Play)
- Search
- Catalog View
- Auto Skip
- Random Play
- Customize Playlist
- Repeat Mode